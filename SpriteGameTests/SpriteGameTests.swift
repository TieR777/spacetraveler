//
//  SpriteGameTests.swift
//  SpriteGameTests
//
//  Created by Nikita Gorobets on 11/02/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import XCTest
@testable import SpriteGame

class SpriteGameTests: XCTestCase {

    override func setUp() {
        
    }

    override func tearDown() {
        
    }

    func testSoundResources() {
        
        XCTAssertNotNil(Bundle.main.path(forResource: "music_menu", ofType: "mp3"), "Menu background music file is missing!")
        
        XCTAssertNotNil(Bundle.main.path(forResource: "music_space", ofType: "mp3"), "Background music file is missing!")
        
        XCTAssertNotNil(Bundle.main.path(forResource: "sound_punch", ofType: "wav"), "Bump sound file is missing!")
        
        XCTAssertNotNil(Bundle.main.path(forResource: "sound_game_over", ofType: "wav"), "Game Over sound file is missing!")
    }
}
