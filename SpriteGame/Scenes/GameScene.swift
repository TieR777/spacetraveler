//
//  GameScene.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 11/02/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit
import GameplayKit
import CoreMotion

protocol GameSceneDelegate {
    
    func gameDidStart()
    func didUpdate(score: Int)
    func needTapticFeedback(isHeavy: Bool)
    func gameDidEnd(score: Int)
}

class GameScene: SKScene {
    
    var gameSceneDelegate: GameSceneDelegate?
    var gameIsOver = false
    var score = 0 {
        
        didSet {
            
            if let gameSceneDelegate = gameSceneDelegate {
                
                gameSceneDelegate.didUpdate(score: score)
            }
        }
    }
    
    
    // MARK: - Events
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        
        setupScene()
    }
    
    
    // MARK: - Scene Objects
    
    open func setupScene() {
        
        if let gameSceneDelegate = gameSceneDelegate {
            
            gameSceneDelegate.gameDidStart()
        }
    }
    
    
    // MARK: - Game State
    
    open func pauseGame() {
        
        isPaused = true
    }
    
    open func continueGame() {
        
        isPaused = false
    }
}
