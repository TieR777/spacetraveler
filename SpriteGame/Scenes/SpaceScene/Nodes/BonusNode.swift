//
//  BonusNode.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class BonusNode: SKSpriteNode, NodeInSpace {
    
    private(set) var bonus: Bonus!
    
    convenience init(type: BonusType) {
        self.init(imageNamed: type.getImageName())
        
        bonus = Bonus(type: type)
        name = Bonus.identifier
        setScale(type == .coin ? 1 : CGFloat.random(in: 1...1.4))
        
        applyDefaultPhysics()
    }
    
    internal func applyDefaultPhysics() {
        
        physicsBody = SKPhysicsBody(circleOfRadius: size.width / 2.2)
        physicsBody?.angularVelocity = CGFloat.random(in: -5.0 ... 5.0)
        physicsBody?.velocity.dx = CGFloat.random(in: -20 ... 20)
        physicsBody?.velocity.dy = CGFloat.random(in: -300 ... -150)
        physicsBody?.mass = 3
        
        physicsBody?.categoryBitMask = NodeCategory.collectable.rawValue
        physicsBody?.collisionBitMask = NodeCategory.none.rawValue
        physicsBody?.contactTestBitMask = NodeCategory.character.rawValue
    }
    
    func collect() {
        
        bonus.collect()
        
        physicsBody = nil
        
        run(SKAction.scale(to: 0, duration: bonus.collectDuration))
        run(SKAction.fadeOut(withDuration: bonus.collectDuration)) {
            [weak self] in
            
            self?.removeFromParent()
        }
    }
}
