//
//  AsteroidShardNode.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class AsteroidShardNode: SKSpriteNode, NodeInSpace {
    
    convenience init(asteroidNode: AsteroidNode) {
        self.init(imageNamed: asteroidNode.asteroid.type.getImageName())
        
        position = asteroidNode.position
        setScale(CGFloat.random(in: asteroidNode.xScale / 3.0 ... asteroidNode.xScale / 2.0))
        
        applyDefaultPhysics()
    }
    
    internal func applyDefaultPhysics() {
        
        physicsBody = SKPhysicsBody(circleOfRadius: size.width / 2.0)
        physicsBody?.angularVelocity = CGFloat.random(in: -5.0 ... 5.0)
        physicsBody?.velocity.dx = CGFloat.random(in: -300 ... 300)
        physicsBody?.velocity.dy = CGFloat.random(in: -300 ... 300)
        physicsBody?.mass = 1
        
        physicsBody?.categoryBitMask = NodeCategory.none.rawValue
    }
    
    func dismiss() {
        
        run(SKAction.scale(to: 0, duration: 1)) {
            [weak self] in
            
            self?.removeFromParent()
        }
    }
}
