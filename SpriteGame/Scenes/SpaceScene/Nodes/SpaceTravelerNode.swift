//
//  SpaceTravelerNode.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class SpaceTravelerNode: SKSpriteNode, NodeInSpace {
    
    private(set) var spaceTraveler: SpaceTraveler!
    private var bodyNode: SKSpriteNode!
    private(set) var engineNode: SKEmitterNode?
    private var movementDirection: SpaceTravelerDirection = .left
    private(set) var isShoked = false
    
    convenience init(type: SpaceTravelerType, healthCarer: HealthCarer?) {
        self.init()
        
        spaceTraveler = SpaceTraveler(type: type, healthCarer: healthCarer)
        name = SpaceTraveler.identifier
        movementDirection = .left
        
        bodyNode = SKSpriteNode(imageNamed: type.getImageNameFor(direction: movementDirection))
        bodyNode.zPosition = Layer.character.rawValue
        bodyNode.setScale(0.8)
        addChild(bodyNode)
        
        applyDefaultPhysics()
    }
    
    internal func applyDefaultPhysics() {
        
        let head = SKPhysicsBody(circleOfRadius: bodyNode.size.width / 3.0, center: CGPoint(x: 0, y: bodyNode.size.height / 4.0))
        let body = SKPhysicsBody(rectangleOf: CGSize(width: bodyNode.size.width * 0.3, height: bodyNode.size.height * 0.3), center: CGPoint(x: 0, y: -bodyNode.size.height / 4.0))
        
        physicsBody = SKPhysicsBody(bodies: [head, body])
        physicsBody?.mass = 10
        physicsBody?.affectedByGravity = false
        physicsBody?.linearDamping = 1
        //physicsBody?.angularDamping = 1
        
        physicsBody?.categoryBitMask = NodeCategory.character.rawValue
        physicsBody?.collisionBitMask = NodeCategory.bounds.rawValue
        physicsBody?.contactTestBitMask = NodeCategory.character.rawValue
    }
    
    func apply(direction: SpaceTravelerDirection) {
        
        guard movementDirection != direction else { return }
        
        bodyNode.texture = SKTexture.init(image: spaceTraveler.type.getImageFor(direction: direction))
        
        movementDirection = direction
    }
    
    func drop(duration: TimeInterval) {
        
        bodyNode.run(SKAction.rotate(toAngle: CGFloat(Double.pi * 2), duration: duration))
    }
    
    func playDead() {
        
        physicsBody?.velocity.dy = -400
        physicsBody?.angularVelocity = CGFloat.random(in: -5 ... 5)
        physicsBody?.affectedByGravity = true
        
        bodyNode.run(SKAction.colorize(with: Color.defaultBlack.uiColor, colorBlendFactor: 1, duration: 0.3))
        
        let bloodEmitter = SKEmitterNode(fileNamed: Emitter.blood.rawValue)
        bloodEmitter?.zPosition = Layer.emitter.rawValue
        bloodEmitter?.name = Emitter.blood.rawValue
        bloodEmitter?.targetNode = parent
        bloodEmitter?.physicsBody?.affectedByGravity = false
        addChild(bloodEmitter!)
        
        stopEngine()
    }
    
    func reset() {
        
        enumerateChildNodes(withName: Emitter.blood.rawValue, using: {
            node, stop in
            
            node.removeFromParent()
        })
        
        physicsBody?.affectedByGravity = false
        
        spaceTraveler.resetHealth()
        
        stopEngine()
    }
}

extension SpaceTravelerNode {
    
    func startEngine() {
        
        guard engineNode?.parent == nil else { return }
        
        if engineNode == nil {
            
            engineNode = SKEmitterNode(fileNamed: Emitter.engine.rawValue)
            engineNode?.zPosition = Layer.emitter.rawValue
            engineNode?.position.y = 0
            engineNode?.targetNode = parent
        }
        
        addChild(engineNode!)
    }
    
    func stopEngine() {
        
        guard let engineNode = engineNode else { return }
        
        engineNode.removeFromParent()
    }
    
    func stabilizeIfNeed() {
        
        guard !isShoked, let angularVelocity = physicsBody?.angularVelocity else { return }
            
        if angularVelocity != 0 || zRotation != 0 {
            
            physicsBody?.angularVelocity = 0
            run(SKAction.rotate(toAngle: 0, duration: 0.3), withKey: "stabRotation")
        }
    }
}

extension SpaceTravelerNode {
    
    private func activateDefence() {
        
        isShoked = true
        removeAction(forKey: "stabRotation")
        engineNode?.particleBirthRate = 30
        
        spaceTraveler.isInvincible = true
        
        let colorChange = SKAction.colorize(with: Color.yellow.uiColor, colorBlendFactor: 1, duration: 0.2)
        let sequence = SKAction.sequence([colorChange, colorChange.reversed()])
        let repeatColor = SKAction.repeat(sequence, count: 5)
        repeatColor.timingMode = .easeOut
        
        bodyNode.run(repeatColor) {
            [weak self] in
            
            self?.isShoked = false
            self?.spaceTraveler.isInvincible = false
            
            self?.engineNode?.particleBirthRate = 150
        }
    }
}

extension SpaceTravelerNode: SpaceTalk {
    
    func say(phrase: String, mood: PhraseMood) {
        
        enumerateChildNodes(withName: "sayLabel") {
            node, stop in
            
            node.run(SKAction.fadeOut(withDuration: 0.1)) {
                node.removeFromParent()
            }
        }
        
        let label = SKLabelNode(fontNamed: Font.computer.rawValue)
        label.zPosition = Layer.ui.rawValue
        label.position = CGPoint(x: bodyNode.size.width / 2.0, y: bodyNode.size.height / 2.0)
        label.fontColor = mood.color.uiColor
        label.fontSize = 16.0
        label.horizontalAlignmentMode = .left
        addChild(label)
        
        var seqArr: [SKAction] = []
        for charIndex in 1...phrase.count {
            
            seqArr.append(contentsOf: [SKAction.run { label.text = String(phrase.prefix(charIndex)) }, SKAction.wait(forDuration: 0.05)])
        }
        
        seqArr.append(SKAction.wait(forDuration: 0.5))
        seqArr.append(SKAction.fadeOut(withDuration: 0.2))
        
        let seq = SKAction.sequence(seqArr)
        seq.timingMode = .easeInEaseOut
        
        label.run(seq, completion: {
            [weak label] in
            
            label?.removeFromParent()
        })
    }
}

extension SpaceTravelerNode {
    
    func reactToCollisionWith(asteroidNode: AsteroidNode) {
        
        asteroidNode.explode()
        
        if DefaultsManager.isSoundEnabled() {
            
            run(SKAction.playSoundFileNamed(spaceTraveler.isAlive() ? Sound.bump8Bit.rawValue : Sound.punch.rawValue, waitForCompletion: false))
        }
        
        guard !spaceTraveler.isInvincible else { return }
        
        spaceTraveler.damage(by: asteroidNode.asteroid.damage)
        
        guard spaceTraveler.isAlive() else { return }
        
        activateDefence()
        say(phrase: AstronautPhrase.fock.phrase, mood: .negative)
    }
    
    func reactToCollisionWith(bonusNode: BonusNode) {
        
        guard let type = bonusNode.bonus.type else { return }
        
        bonusNode.collect()
        bonusNode.run(SKAction.move(to: position, duration: bonusNode.bonus.collectDuration))
        
        let mood: PhraseMood!
        
        switch type {
        case .coin, .cheapMineral, .normalMineral: mood = .neutral
        case .expensiveMineral, .gem: mood = .cheerUp
        }
        
        say(phrase: "+\(bonusNode.bonus.type.value)", mood: mood)
        
        if DefaultsManager.isSoundEnabled() {
            
            run(SKAction.playSoundFileNamed(Sound.coin.rawValue, waitForCompletion: false))
        }
    }
    
    func reactToCollisionWith(healthPackNode: HealthPackNode) {
        
        guard let healthPack = healthPackNode.healthPack, let health = healthPack.health else { return }
        
        healthPackNode.collect()
        healthPackNode.run(SKAction.move(to: position, duration: healthPack.collectDuration))
        
        spaceTraveler.heal(by: health)
        say(phrase: "+\(health)", mood: .positive)
        
        if DefaultsManager.isSoundEnabled() {
            
            run(SKAction.playSoundFileNamed(Sound.life.rawValue, waitForCompletion: false))
        }
    }
}
