//
//  StarLayerNode.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 26/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class StarLayerNode: SKSpriteNode {
    
    convenience init(size: CGSize) {
        self.init()
        
        self.size = size
    }
    
    func launchStars() {
        
        guard children.isEmpty else { return }
        
        for i in 1...5 {
            
            if let starsEmitter = SKEmitterNode(fileNamed: Emitter.stars.rawValue) {
                
                starsEmitter.particleTexture = SKTexture(imageNamed: "star\(i)")
                starsEmitter.zPosition = 1
                starsEmitter.position = CGPoint(x: 0, y: size.height / 2 + 5)
                starsEmitter.particlePositionRange.dx = size.width + 10
                starsEmitter.advanceSimulationTime(50)
                
                addChild(starsEmitter)
            }
        }
    }
    
    func stopStars() {
        
        children.forEach { $0.removeFromParent() }
    }
}
