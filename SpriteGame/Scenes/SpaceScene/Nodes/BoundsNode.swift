//
//  BoundsNode.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 01/04/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class BoundsNode: SKNode {
    
    convenience init(parentFrame: CGRect) {
        self.init()
        
        physicsBody = SKPhysicsBody(edgeLoopFrom: CGRect(x: parentFrame.minX - 8, y: parentFrame.minY - 8, width: parentFrame.width + 16, height: parentFrame.height + 16))
        physicsBody?.friction = 0
        
        physicsBody?.categoryBitMask = NodeCategory.bounds.rawValue
        physicsBody?.collisionBitMask = NodeCategory.none.rawValue
        physicsBody?.contactTestBitMask = NodeCategory.none.rawValue
    }
}
