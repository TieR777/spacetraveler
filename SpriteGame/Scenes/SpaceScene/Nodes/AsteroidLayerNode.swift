//
//  AsteroidLayerNode.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 26/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class AsteroidLayerNode: SKSpriteNode {
    
    convenience init(size: CGSize) {
        self.init()
        
        self.size = size
    }
    
    func clearGarbage() {
        
        enumerateChildNodes(withName: Asteroid.identifier, using: {
            asteroid, stop in
            
            if asteroid.position.y <= self.frame.minY - asteroid.frame.height {
                
                asteroid.removeFromParent()
            }
        })
        
        enumerateChildNodes(withName: Bonus.identifier, using: {
            coin, stop in
            
            if coin.position.y <= self.frame.minY - coin.frame.height {
                
                coin.removeFromParent()
            }
        })
        
        enumerateChildNodes(withName: HealthPack.identifier, using: {
            heart, stop in
            
            if heart.position.y <= self.frame.minY - heart.frame.height {
                
                heart.removeFromParent()
            }
        })
    }
}
