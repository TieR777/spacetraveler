//
//  HealthPackNode.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class HealthPackNode: SKSpriteNode, NodeInSpace {
    
    private(set) var healthPack: HealthPack!
    
    convenience init() {
        self.init(imageNamed: "soup")
        
        healthPack = HealthPack(health: 1)
        name = HealthPack.identifier
        
        applyDefaultPhysics()
    }
    
    internal func applyDefaultPhysics() {
        
        physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: size.width, height: size.height))
        physicsBody?.angularVelocity = CGFloat.random(in: -3.0 ... 3.0)
        physicsBody?.velocity.dx = CGFloat.random(in: -10 ... 10)
        physicsBody?.velocity.dy = CGFloat.random(in: -200 ... -50)
        physicsBody?.mass = 2
        
        physicsBody?.categoryBitMask = NodeCategory.collectable.rawValue
        physicsBody?.collisionBitMask = NodeCategory.none.rawValue
        physicsBody?.contactTestBitMask = NodeCategory.character.rawValue
    }
    
    func collect() {
        
        healthPack.collect()
        
        physicsBody = nil
        
        run(SKAction.scale(to: 0, duration: healthPack.collectDuration))
        run(SKAction.fadeOut(withDuration: healthPack.collectDuration)) {
            [weak self] in
            
            self?.removeFromParent()
        }
    }
}
