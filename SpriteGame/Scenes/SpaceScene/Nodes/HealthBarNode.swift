//
//  HealthBarNode.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 18/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class HealthBarNode: SKSpriteNode {
    
    private var maxHealth: Int!
    private var currentHealth: Int!
    
    convenience init(maxHealth: Int) {
        self.init()
        
        self.maxHealth = maxHealth
        self.currentHealth = maxHealth
        
        let tempItem = createItem(i: 0)
        size = CGSize(width: tempItem.size.width * CGFloat(maxHealth) + CGFloat(maxHealth - 1) * 12.0, height: tempItem.size.height)
        
        reset()
    }
    
    private func createItem(i: Int) -> SKSpriteNode {
        
        let barItemNode = HealthBarItem(index: i)
        
        let halfParentWidth: CGFloat = size.width / 2.0
        let halfWidth: CGFloat = barItemNode.size.width / 2.0
        
        if children.isEmpty {
            
            barItemNode.position.x = -halfParentWidth + halfWidth
            
        } else {
            
            barItemNode.position.x = children[i - 1].frame.maxX + 12.0 + halfWidth
        }
        
        return barItemNode
    }
    
    func update(newHealth: Int) {
        
        guard newHealth <= maxHealth else { return }
        
        if newHealth >= currentHealth {
            
            for i in currentHealth..<newHealth {
                
                addChild(createItem(i: i))
            }
            
        } else {
            
            for (i, item) in children.enumerated() {
                
                if i >= newHealth {
                    
                    item.removeFromParent()
                }
            }
        }
        
        self.currentHealth = newHealth
    }
    
    func reset() {
        
        removeAllChildren()
        
        currentHealth = 0
        
        update(newHealth: maxHealth)
    }
}

class HealthBarItem: SKSpriteNode {
    
    convenience init(index: Int) {
        self.init(imageNamed: "soup")
        
        //setScale(1.2)
    }
    
    override func removeFromParent() {
        
        run(SKAction.fadeOut(withDuration: 0.3))
        run(SKAction.resize(toWidth: size.width, height: size.height * 0.1, duration: 0.3)) {
            
            super.removeFromParent()
        }
    }
}
