//
//  Asteroidswift
//  SpriteGame
//
//  Created by Nikita Gorobets on 05/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class AsteroidNode: SKSpriteNode, NodeInSpace {
    
    private(set) var asteroid: Asteroid!
    private var trailEmitter: SKEmitterNode?
    
    convenience init(type: AsteroidType) {
        self.init(imageNamed: type.getImageName())
        
        let scale = CGFloat.random(in: 0.5 ... 3)
        
        asteroid = Asteroid(type: type, damage: scale > 2 ? 2 : 1)
        name = Asteroid.identifier
        setScale(scale)
        
        applyDefaultPhysics()
    }
    
    internal func applyDefaultPhysics() {
        
        physicsBody = SKPhysicsBody(circleOfRadius: size.width / 2.2)
        physicsBody?.angularVelocity = CGFloat.random(in: -3.0 ... 3.0)
        physicsBody?.velocity.dx = CGFloat.random(in: -40 ... 40)
        physicsBody?.velocity.dy = CGFloat.random(in: -500 ... -300)
        physicsBody?.mass = 100 * xScale
        
        physicsBody?.categoryBitMask = NodeCategory.damaging.rawValue
        physicsBody?.collisionBitMask = NodeCategory.character.rawValue
        physicsBody?.contactTestBitMask = NodeCategory.character.rawValue
    }
    
    func addTrail() {
        
        guard trailEmitter == nil else { return }
        
        trailEmitter = SKEmitterNode(fileNamed: Emitter.asteroidTrail.rawValue)
        trailEmitter?.setScale(xScale)
        trailEmitter?.zPosition = Layer.background.rawValue
        //trailEmitter?.particleBirthRate = xScale * trailEmitter!.particleBirthRate
        //trailEmitter?.yAcceleration = xScale * trailEmitter!.yAcceleration
        //trailEmitter?.speed = xScale * trailEmitter!.speed
        //trailEmitter?.particleLifetime = xScale * trailEmitter!.particleLifetime
        //trailEmitter?.particleScaleSpeed = xScale * trailEmitter!.particleScaleSpeed
        trailEmitter?.targetNode = parent
        addChild(trailEmitter!)
        
        let shim1 = SKAction.moveTo(x: position.x + 1.5, duration: 0.1)
        let shim2 = SKAction.moveTo(x: position.x - 1.5, duration: 0.1)
        let sequence = SKAction.sequence([shim1, shim2])
        let repeatShim = SKAction.repeatForever(sequence)
        
        run(repeatShim)
    }
    
    func explode() {
        
        asteroid.collapse()
        
        physicsBody?.collisionBitMask = NodeCategory.none.rawValue
        physicsBody?.contactTestBitMask = NodeCategory.none.rawValue
        
        let explosionEmitter = SKEmitterNode(fileNamed: Emitter.explosion.rawValue)
        explosionEmitter?.zPosition = -1
        explosionEmitter?.position = position
        explosionEmitter?.targetNode = parent
        explosionEmitter?.setScale(xScale)
        explosionEmitter?.physicsBody?.affectedByGravity = false
        parent?.addChild(explosionEmitter!)
        
        dropShards()
        
        removeFromParent()
    }
    
    private func dropShards() {
        
        for _ in 1 ... Int.random(in: 3 ... 5) {
            
            let shardNode = AsteroidShardNode(asteroidNode: self)
            parent?.addChild(shardNode)
            shardNode.dismiss()
        }
    }
}
