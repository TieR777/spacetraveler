//
//  NodeInSpace.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import Foundation

protocol NodeInSpace {
    
    func applyDefaultPhysics()
}
