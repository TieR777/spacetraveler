//
//  SpaceTalk.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

enum PhraseMood {
    
    case positive
    case neutral
    case cheerUp
    case negative
    
    var color: Color {
        
        switch self {
        case .positive: return Color.green
        case .neutral: return Color.white
        case .cheerUp: return Color.yellow
        case .negative: return Color.red
        }
    }
}

protocol SpacePhrase {
    
    var phrase: String { get }
    var varialPhrase: String { get }
}

enum AstronautPhrase: SpacePhrase {
    
    case ok
    case fock
    case good
    case go
    
    var phrase: String {
        
        switch self {
        case .ok: return "OK"
        case .fock: return "F#&K!1"
        case .good: return "Nice!"
        case .go: return "POGNALI NAHUI!1"
        }
    }
    
    var varialPhrase: String {
        
        return "Varial Phrase"
    }
}

protocol SpaceTalk {
    
    func say(phrase: String, mood: PhraseMood)
}
