//
//  HealthCarer.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 31/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import Foundation

protocol HealthCarer {
    
    func healthChanged(patient: HealthCare, feedbackLevel: Int)
    func deadOccurs(patient: HealthCare)
    func resurectionOccurs(patient: HealthCare)
}
