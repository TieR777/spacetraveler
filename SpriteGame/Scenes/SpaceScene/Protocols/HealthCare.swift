//
//  HealthCare.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import Foundation

protocol HealthCare {
    
    var healthLeft: Int { get }
    var isInvincible: Bool { get }
    var healthCarerDelegate: HealthCarer? { get }
    
    func resetHealth()
    func damage(by: Int)
    func heal(by: Int)
    func isAlive() -> Bool
    func isFullHealth() -> Bool
}
