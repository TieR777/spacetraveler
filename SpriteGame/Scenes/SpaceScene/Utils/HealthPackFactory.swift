//
//  HealthPackFactory.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 31/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class HealthPackFactory {
    
    func startLaunchHealthPacks(spawnNode: SKNode) {
        
        let creation = SKAction.run {
            
            let healthPackNode = HealthPackNode()
            let doubleWidth = healthPackNode.size.height * 2.0
            healthPackNode.position.x = CGFloat.random(in: (spawnNode.frame.minX + doubleWidth)...(spawnNode.frame.maxX - doubleWidth))
            healthPackNode.position.y = spawnNode.frame.maxY + healthPackNode.size.height
            healthPackNode.zPosition = 2
            
            spawnNode.addChild(healthPackNode)
        }
        
        let delay = SKAction.wait(forDuration: TimeInterval.random(in: 20 ... 40), withRange: 1)
        let sequence = SKAction.sequence([delay, creation, delay])
        let repeatCreation = SKAction.repeatForever(sequence)
        
        spawnNode.run(repeatCreation, withKey: "spawnHealthPacks")
    }
    
    func stop(spawnNode: SKNode) {
        
        spawnNode.removeAction(forKey: "spawnHealthPacks")
    }
}
