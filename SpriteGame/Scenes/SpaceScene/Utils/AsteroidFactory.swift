//
//  AsteroidFactory.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 31/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class AsteroidFactory {
    
    private var asteroidCounter = 0
    private var asteroidDensity = 1
    
    func start(spawnNode: SKNode) {
        
        let creation = SKAction.run {
            
            let asteroidType = AsteroidType(rawValue: Int.random(in: AsteroidType.one.rawValue ... AsteroidType.five.rawValue))!
            let asteroidNode = AsteroidNode(type: asteroidType)
            let doubleWidth = asteroidNode.size.width / 2.0
            asteroidNode.position.x = CGFloat.random(in: (spawnNode.frame.minX + doubleWidth)...(spawnNode.frame.maxX - doubleWidth))
            asteroidNode.position.y = spawnNode.frame.maxY + asteroidNode.size.height
            asteroidNode.zPosition = 2
            
            spawnNode.addChild(asteroidNode)
            asteroidNode.addTrail()
        }
        
        let delay = SKAction.wait(forDuration: 0.4)
        
        let repeatAction = SKAction.run {
            [weak self] in
            
            guard let self = self else { return }
            
            let asteroidCount = spawnNode.children.filter({ $0.name == Asteroid.identifier }).count
            
            if asteroidCount < self.asteroidDensity {
                
                let threshhold = self.asteroidDensity > 3 ? Int.random(in: 0...1) : 0
                
                for _ in 0...threshhold {
                    
                    spawnNode.run(creation)
                }
            }
            
            self.asteroidCounter += 1
            
            if self.asteroidCounter > 100 {
                
                self.asteroidCounter = 0
                self.asteroidDensity += 1
                
                if self.asteroidDensity >= 5 {
                    
                    self.asteroidDensity = Int.random(in: 1...5)
                }
            }
        }
        
        let sequence = SKAction.sequence([delay, repeatAction])
        let repeatCreation = SKAction.repeatForever(sequence)
        
        spawnNode.run(repeatCreation, withKey: "spawnAsteroids")
    }
    
    func stop(spawnNode: SKNode) {
        
        spawnNode.removeAction(forKey: "spawnAsteroids")
    }
}
