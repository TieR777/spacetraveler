//
//  SpaceFactory.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class SpaceFactory {
    
    private let spawnNode: SKNode!
    private let asteroidFactory = AsteroidFactory()
    private let bonusFactory = BonusFactory()
    private let healthPackFactory = HealthPackFactory()
    
    var canLaunchHealthPacks = false {
        
        didSet {
            
            if canLaunchHealthPacks {
                
                healthPackFactory.startLaunchHealthPacks(spawnNode: spawnNode)
                
            } else {
                
                healthPackFactory.stop(spawnNode: spawnNode)
            }
        }
    }
    
    init(spawnNode: SKNode) {
        
        self.spawnNode = spawnNode
    }
    
    func startLaunchingSpaceObjects() {
        
        asteroidFactory.start(spawnNode: spawnNode)
        bonusFactory.start(spawnNode: spawnNode)
    }
    
    func invalidate() {
        
        asteroidFactory.stop(spawnNode: spawnNode)
        bonusFactory.stop(spawnNode: spawnNode)
        healthPackFactory.stop(spawnNode: spawnNode)
    }
}
