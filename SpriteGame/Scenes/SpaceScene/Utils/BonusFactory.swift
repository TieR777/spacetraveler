//
//  BonusFactory.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 31/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class BonusFactory {
    
    func start(spawnNode: SKNode) {
        
        let creation = SKAction.run {
            
            let bonusType = BonusType(rawValue: Int.random(in: BonusType.coin.rawValue ... BonusType.gem.rawValue))!
            let bonusNode = BonusNode(type: bonusType)
            let doubleWidth = bonusNode.size.width * 1
            bonusNode.position.x = CGFloat.random(in: (spawnNode.frame.minX + doubleWidth)...(spawnNode.frame.maxX - doubleWidth))
            bonusNode.position.y = spawnNode.frame.maxY + bonusNode.size.height
            bonusNode.zPosition = 2
            
            spawnNode.addChild(bonusNode)
        }
        
        let delay = SKAction.wait(forDuration: 2, withRange: 1)
        let sequence = SKAction.sequence([delay, creation, delay])
        let repeatCreation = SKAction.repeatForever(sequence)
        
        spawnNode.run(repeatCreation, withKey: "spawnBonuses")
    }
    
    func stop(spawnNode: SKNode) {
        
        spawnNode.removeAction(forKey: "spawnBonuses")
    }
}
