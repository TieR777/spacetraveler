//
//  SpaceScene.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 14/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import CoreMotion

class SpaceScene: GameScene {
    
    private lazy var spaceFactory = SpaceFactory(spawnNode: asteroidLayer)
    private var boundsNode: BoundsNode!
    private var starLayer: StarLayerNode!
    private var asteroidLayer: AsteroidLayerNode!
    private var healthBarNode: HealthBarNode!
    private var spaceTravelerNode: SpaceTravelerNode!
    private let motionManager = { () -> CMMotionManager in
        
        let motionManager = CMMotionManager()
        motionManager.accelerometerUpdateInterval = 0.1
        
        return motionManager
    }()
    private var isStarted = false
    
    
    // MARK: - Events
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        
        startOn(count: 3)
    }
    
    override func didSimulatePhysics() {

        asteroidLayer.clearGarbage()
    }
    
    
    // MARK: - Nodes
    
    override func setupScene() {
        
        backgroundColor = Color.defaultBlack.uiColor
        
        setupPhysics()
        addStarLayer()
        addAsteroidsLayer()
        addSpaceTraveler()
        
        super.setupScene()
    }
    
    private func setupPhysics() {
        
        physicsWorld.contactDelegate = self
        physicsWorld.gravity = CGVector(dx: 0, dy: -0.8)
    }
    
    private func addStarLayer() {
        
        starLayer = StarLayerNode(size: size)
        starLayer.zPosition = Layer.background.rawValue
        addChild(starLayer)
        
        starLayer.launchStars()
    }
    
    private func addAsteroidsLayer() {
        
        asteroidLayer = AsteroidLayerNode(size: size)
        asteroidLayer.zPosition = Layer.objects.rawValue
        addChild(asteroidLayer)
    }
    
    private func addBounds() {
        
        boundsNode = BoundsNode(parentFrame: frame)
        boundsNode.zPosition = Layer.ui.rawValue
        boundsNode.position = .zero
        addChild(boundsNode)
    }
    
    private func addSpaceTraveler() {
        
        spaceTravelerNode = SpaceTravelerNode(type: .astronaut, healthCarer: self)
        spaceTravelerNode.zPosition = Layer.character.rawValue
        asteroidLayer.addChild(spaceTravelerNode)
    }
    
    private func addHealthBar(maxHealth: Int) {
        
        healthBarNode = HealthBarNode(maxHealth: maxHealth)
        healthBarNode.zPosition = Layer.ui.rawValue
        addChild(healthBarNode)
        
        healthBarNode.position = CGPoint(x: frame.minX + healthBarNode.size.width / 2.0 + 16.0, y: frame.minY + healthBarNode.size.height + 16.0)
    }
    
    private func dropOut(spaceTravelerNode: SpaceTravelerNode, duration: TimeInterval) {
        
        spaceTravelerNode.position = CGPoint(x: 0, y: frame.minY - spaceTravelerNode.size.height)
        
        spaceTravelerNode.drop(duration: duration)
        spaceTravelerNode.run(SKAction.move(to: CGPoint.zero, duration: duration), completion: {
            [weak self] in
            
            spaceTravelerNode.startEngine()
            spaceTravelerNode.say(phrase: AstronautPhrase.go.phrase, mood: .cheerUp)
            spaceTravelerNode.say(phrase: AstronautPhrase.go.phrase, mood: .cheerUp)
            spaceTravelerNode.say(phrase: AstronautPhrase.go.phrase, mood: .cheerUp)
            
            self?.startGame()
        })
    }
    
    private func showCountdownLabel(count: Int) {
        
        let label = SKLabelNode(fontNamed: Font.computer.rawValue)
        label.zPosition = Layer.ui.rawValue
        label.fontColor = Color.white.uiColor
        label.fontSize = 40.0
        addChild(label)
        
        var seqArr: [SKAction] = []
        
        for i in stride(from: count, to: 0, by: -1) {
            
            seqArr.append(contentsOf: [SKAction.run({ label.text = String(i) }), SKAction.scale(to: 4, duration: 0.25), SKAction.fadeIn(withDuration: 0.25), SKAction.wait(forDuration: 0.5), SKAction.fadeOut(withDuration: 0.25), SKAction.scale(to: 0, duration: 0)])
        }
        
        let seq = SKAction.sequence(seqArr)
        label.run(seq, completion: {
            
            label.removeFromParent()
        })
    }
    
    
    // MARK: - Controls
    
    private func activateControls() {
        
        guard !motionManager.isAccelerometerActive else { return }
        
        motionManager.startAccelerometerUpdates(to: OperationQueue.current!, withHandler: {
            [weak self] data, error in
            
            guard let self = self, !self.isPaused, let data = data else { return }
            
            var x = CGFloat(data.acceleration.x)
            var z = CGFloat(data.acceleration.y - data.acceleration.z)
            
            var shiftX = x * 5
            shiftX.negate()
            var shiftY = z * 5
            shiftY.negate()
            self.starLayer.run(SKAction.move(by: CGVector(dx: shiftX, dy: shiftY), duration: 0.5))
            
            let speed = self.spaceTravelerNode.spaceTraveler.type.engineSpeed
            let acceleration: CGFloat = 100
            x *= speed * 1.5 * acceleration
            z *= speed * acceleration / 2.0
            
            self.spaceTravelerNode.physicsBody?.applyForce(CGVector(dx: x, dy: z))
            self.spaceTravelerNode.apply(direction: x < 0 ? .left : .right)
            
            self.spaceTravelerNode.stabilizeIfNeed()
        })
    }
    
    private func deactivateControls() {
        
        motionManager.stopAccelerometerUpdates()
    }
    
    
    // MARK: - Gameplay
    
    private func startOn(count: Int) {
        
        showCountdownLabel(count: count)
        dropOut(spaceTravelerNode: spaceTravelerNode, duration: TimeInterval(count + 1))
    }
    
    private func startGame() {
        
        isStarted = true
        
        activateControls()
        
        addBounds()
        
        spaceFactory.startLaunchingSpaceObjects()
    }
    
    private func startGameOverScenario() {
        
        guard !gameIsOver else { return }
        
        gameIsOver = true
        
        spaceFactory.invalidate()
        
        boundsNode.removeFromParent()
        
        deactivateControls()
        
        if DefaultsManager.isSoundEnabled() {
            
            run(SKAction.playSoundFileNamed(Sound.gameOver.rawValue, waitForCompletion: false))
        }
        
        if let gameSceneDelegate = gameSceneDelegate {
            
            gameSceneDelegate.gameDidEnd(score: score)
        }
    }
}

extension SpaceScene: SKPhysicsContactDelegate {
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        guard isStarted, !gameIsOver else { return }
        
        #if DEBUG
        print("A: \(contact.bodyA.node?.name ?? "") | B: \(contact.bodyB.node?.name ?? "")")
        #endif
        
        let nodes = [contact.bodyA.node, contact.bodyB.node]
        
        guard let travelerNode = nodes.filter({ $0 is SpaceTravelerNode }).first as? SpaceTravelerNode, let collideNode = nodes.filter({ $0?.name != SpaceTraveler.identifier }).first else { return }
        
        switch collideNode {
        case let healthPackNode as HealthPackNode:
            
            guard !healthPackNode.healthPack.isCollected else { return }
            
            travelerNode.reactToCollisionWith(healthPackNode: healthPackNode)
            
        case let bonusNode as BonusNode:
            
            guard !bonusNode.bonus.isCollected else { return }
            
            if let gameSceneDelegate = gameSceneDelegate {
                
                gameSceneDelegate.needTapticFeedback(isHeavy: false)
            }
            
            travelerNode.reactToCollisionWith(bonusNode: bonusNode)
            
            score += bonusNode.bonus.type.value
            
        case let asteroidNode as AsteroidNode:
        
            guard !asteroidNode.asteroid.isCollapsed else { return }
            
            travelerNode.reactToCollisionWith(asteroidNode: asteroidNode)
            
        default: break
        }
    }
}

extension SpaceScene: HealthCarer {
    
    func healthChanged(patient: HealthCare, feedbackLevel: Int) {
        
        spaceFactory.canLaunchHealthPacks = !patient.isFullHealth()
        
        if let spaceTraveler = patient as? SpaceTraveler {
            
            healthBarNode.update(newHealth: spaceTraveler.healthLeft)
        }
        
        if feedbackLevel > 0, let gameSceneDelegate = gameSceneDelegate {
            
            gameSceneDelegate.needTapticFeedback(isHeavy: feedbackLevel > 1)
        }
    }
    
    func deadOccurs(patient: HealthCare) {
    
        if let _ = patient as? SpaceTraveler {
            
            spaceTravelerNode.playDead()
            
            startGameOverScenario()
        }
    }
    
    func resurectionOccurs(patient: HealthCare) {
        
        guard healthBarNode != nil  else {
            
            addHealthBar(maxHealth: patient.healthLeft)
            
            return
        }
        
        healthBarNode.reset()
    }
}
