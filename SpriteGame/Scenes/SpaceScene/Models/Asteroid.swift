//
//  Asteroid.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import Foundation

class Asteroid {
    
    static let identifier = "asteroid"
    
    private(set) var type: AsteroidType!
    private(set) var damage: Int!
    private(set) var isCollapsed = false
    
    init(type: AsteroidType, damage: Int) {
        
        self.type = type
        self.damage = damage
    }
    
    func collapse() {
        
        isCollapsed = true
    }
}
