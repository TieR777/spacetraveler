//
//  BonusType.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import UIKit

enum BonusType: Int {
    
    case coin
    case cheapMineral
    case normalMineral
    case expensiveMineral
    case gem
    
    func getImageName() -> String {
        
        return Bonus.identifier + String(rawValue + 1)
    }
    
    func getImage() -> UIImage? {
        
        return UIImage(named: getImageName())
    }
    
    var value: Int {
        
        switch self {
        case .coin: return 1
        case .cheapMineral: return 3
        case .normalMineral: return 5
        case .expensiveMineral: return 7
        case .gem: return 10
        }
    }
}
