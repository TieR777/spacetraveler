//
//  Bonus.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import Foundation

class Bonus: CollectableItem {
    
    static let identifier = "bonus"
    
    private(set) var type: BonusType!
    private(set) var isCollected = false
    private(set) var collectDuration: TimeInterval = 0.3
    
    init(type: BonusType) {
        
        self.type = type
    }
    
    func collect() {
        
        isCollected = true
    }
}
