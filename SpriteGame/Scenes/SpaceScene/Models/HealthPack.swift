//
//  HealthPack.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import Foundation

class HealthPack: CollectableItem {
    
    static let identifier = "healthPack"
    
    private(set) var health: Int!
    private(set) var isCollected = false
    private(set) var collectDuration: TimeInterval = 0.3
    
    init(health: Int) {
        
        self.health = health
    }
    
    func collect() {
        
        isCollected = true
    }
}
