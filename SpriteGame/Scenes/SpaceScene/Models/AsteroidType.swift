//
//  AsteroidType.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import UIKit

enum AsteroidType: Int {
    
    case one
    case two
    case three
    case four
    case five
    
    func getImageName() -> String {
        
        return Asteroid.identifier + String(rawValue + 1)
    }
    
    func getImage() -> UIImage? {
        
        return UIImage(named: getImageName())
    }
}
