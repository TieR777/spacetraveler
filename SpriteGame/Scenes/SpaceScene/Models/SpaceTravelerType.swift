//
//  SpaceTravelerType.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import UIKit

enum SpaceTravelerType {
    
    case astronaut
    case spaceship
    
    func getImageNameFor(direction: SpaceTravelerDirection) -> String {
        
        switch self {
        case .astronaut:
            switch direction {
            case .left: return "astronautLeft"
            case .right: return "astronautRight"
            }
        case .spaceship: return ""
        }
    }
    
    func getImageFor(direction: SpaceTravelerDirection) -> UIImage {
        
        return UIImage(named: getImageNameFor(direction: direction))!
    }
    
    var health: Int {
        
        switch self {
        case .astronaut: return 3
        case .spaceship: return 5
        }
    }
    
    var engineSpeed: CGFloat {
        
        switch self {
        case .astronaut: return 1000
        case .spaceship: return 2000
        }
    }
}

enum SpaceTravelerDirection {
    
    case left
    case right
}
