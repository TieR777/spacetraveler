//
//  SpaceTraveler.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 25/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import Foundation

class SpaceTraveler {
    
    static let identifier = "traveler"
    
    private(set) var type: SpaceTravelerType!
    
    internal var healthCarerDelegate: HealthCarer?
    internal var healthLeft: Int = 0
    internal var isInvincible: Bool = false
    
    init(type: SpaceTravelerType, healthCarer: HealthCarer?) {
        
        self.type = type
        self.healthCarerDelegate = healthCarer
        
        resetHealth()
    }
}

extension SpaceTraveler: HealthCare {
    
    func resetHealth() {
        
        healthLeft = type.health
        
        if let delegate = healthCarerDelegate {
            
            delegate.resurectionOccurs(patient: self)
        }
    }
    
    func damage(by: Int) {
        
        healthLeft -= by
        
        if let delegate = healthCarerDelegate {
            
            delegate.healthChanged(patient: self, feedbackLevel: by)
            
            if !isAlive() {
                
                delegate.deadOccurs(patient: self)
            }
        }
    }
    
    func heal(by: Int) {
        
        healthLeft += by
        
        if healthLeft > type.health {
            
            healthLeft = type.health
        }
        
        if let delegate = healthCarerDelegate {
            
            delegate.healthChanged(patient: self, feedbackLevel: 1)
        }
    }

    func isAlive() -> Bool {
        
        return healthLeft > 0
    }
    
    func isFullHealth() -> Bool {
        
        return healthLeft >= type.health
    }
}
