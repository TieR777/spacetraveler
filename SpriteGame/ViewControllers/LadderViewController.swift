//
//  LadderViewController.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 05/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import UIKit

class LadderViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noticeLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private lazy var currentUser = DefaultsManager.getCurrentUser()
    private lazy var score: [(uid: String, score: Int)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if score.isEmpty {
            
            getLadder()
        }
    }
    
    
    // MARK: - Views
    
    private func setupViews() {
        
        noticeLabel.isHidden = true
        tableView.alpha = 0
    }
    
    private func updateTableView() {
        
        tableView.reloadData()
        
        noticeLabel.isHidden = !score.isEmpty
        
        if !score.isEmpty && tableView.alpha == 0 {
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.tableView.alpha = 1
            })
            
            if let currentUserScoreIndex = score.firstIndex(where: { $0.uid == currentUser?.uid }) {
                
                tableView.scrollToRow(at: IndexPath(row: currentUserScoreIndex, section: 0), at: .middle, animated: true)
            }
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "unwindSegue", sender: nil)
    }
    
    
    // MARK: - Data
    
    private func getLadder() {
        
        activityIndicator.startAnimating()
        
        DatabaseManager.getLadder(complition: {
            result in
            
            self.activityIndicator.stopAnimating()
            
            if let result = result?.sorted(by: { $0.score > $1.score }) {
                
                self.score = result
                
                self.updateTableView()
            }
        })
    }
}

extension LadderViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return score.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LadderCell", for: indexPath)
        
        let item = score[indexPath.row]
        
        (cell.viewWithTag(1) as! UILabel).text = Utils.getTempUsername(uid: item.uid)
        (cell.viewWithTag(2) as! UILabel).text = String(item.score)
        
        for case let label as UILabel in cell.contentView.subviews {
            
            label.textColor = currentUser?.uid == item.uid ? Color.yellow.uiColor : Color.white.uiColor
        }
        
        return cell
    }
}
