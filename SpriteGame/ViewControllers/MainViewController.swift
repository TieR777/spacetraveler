//
//  MainViewController.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 04/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import AVFoundation
import FirebaseAuth

class MainViewController: MenuBasedViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var audioPlayer: AVAudioPlayer!
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSoundEffects()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Auth.auth().currentUser == nil {
            
            authUser()
            
        } else {
            
            updateViews()
            
            showMenu()
            
            if !audioPlayer.isPlaying {
                
                audioPlayer.playMusic(needsRestart: true)
            }
        }
    }
    
    
    // MARK: - Views
    
    override func setupViews() {
        super.setupViews()
        
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2.0
        avatarImageView.layer.borderColor = Color.defaultWhite.cgColor
        avatarImageView.layer.borderWidth = 2.0
    }
    
    override func updateViews() {
        super.updateViews()
        
        nameLabel.text = Utils.getTempUsername(uid: DefaultsManager.getCurrentUser()?.uid)
    }
    
    
    // MARK: - Sound & effects
    
    private func setupSoundEffects() {
        
        audioPlayer = AVAudioPlayer.createWith(musicFileName: Music.mainMenu.rawValue)
    }
    
    
    // MARK: - Actions
    
    @IBAction func ladderButtonTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "ladderSegue", sender: nil)
    }
    
    @IBAction func settingsButtonTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "settingsSegue", sender: nil)
    }
    
    @IBAction func playButtonTapped(_ sender: Any) {
        
        audioPlayer.stopMusic()
        
        performSegue(withIdentifier: "gameStartSegue", sender: nil)
    }
    
    
    // MARK: - Auth
    
    private func authUser() {
        
        activityIndicator.startAnimating()
        
        Auth.auth().signInAnonymously(completion: {
            result, error in
            
            self.activityIndicator.stopAnimating()
            
            if let error = error {
                
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                self.present(alert, animated: true, completion: nil)
                
            } else if let user = result?.user {
                
                DefaultsManager.setCurrentUser(user)
                
                self.updateViews()
                
                self.showMenu()
                
                self.audioPlayer.playMusic(needsRestart: true)
            }
        })
    }
}
