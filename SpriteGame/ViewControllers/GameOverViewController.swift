//
//  GameOverViewController.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 04/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import UIKit

protocol GameOverViewControllerDelegate {
    
    func menuButtonTapped()
    func restartButtonTapped()
}

class GameOverViewController: MenuBasedViewController {
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    var delegate: GameOverViewControllerDelegate?
    var score = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        saveLastGameScore()
    }
    
    override func setupViews() {
        
        scoreLabel.text = String(score)
    }
    
    
    // MARK: - Actions
    
    @IBAction func menuButtonTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: {
            
            if let delegate = self.delegate {
                
                delegate.menuButtonTapped()
            }
        })
    }
    
    @IBAction func restartButtonTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: {
            
            if let delegate = self.delegate {
                
                delegate.restartButtonTapped()
            }
        })
    }
    
    @IBAction func ladderButtonTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "ladderSegue", sender: nil)
    }
    
    
    // MARK: - Data
    
    private func saveLastGameScore() {
        
        if let user = DefaultsManager.getCurrentUser() {
            
            DatabaseManager.saveLastGame(score: score, user: user)
        }
    }
}
