//
//  PauseViewController.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 20/02/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import UIKit

protocol PauseViewControllerDelegate {
    
    func menuButtonTapped()
    func restartButtonTapped()
    func resumeButtonTapped()
}

class PauseViewController: MenuBasedViewController {
    
    var delegate: PauseViewControllerDelegate?
    
    
    // MARK: - Views
    
    override func setupViews() {
        
    }
    
    
    // MARK: - Actions

    @IBAction func menuButtonTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
        if let delegate = self.delegate {
            
            delegate.menuButtonTapped()
        }
    }
    
    @IBAction func restartButtonTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: {
            
            if let delegate = self.delegate {
                
                delegate.restartButtonTapped()
            }
        })
    }
    
    @IBAction func resumeButtonTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: {
            
            if let delegate = self.delegate {
                
                delegate.resumeButtonTapped()
            }
        })
    }
    
    @IBAction func settingsButtonTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "settingsSegue", sender: nil)
    }
}
