//
//  GameViewController.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 11/02/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import AVFoundation

class GameViewController: UIViewController {

    @IBOutlet weak var sceneView: SKView!
    @IBOutlet weak var stateButton: UIButton!
    @IBOutlet weak var scoreLabel: UILabel!
    
    private var gameScene: GameScene!
    private var audioPlayer: AVAudioPlayer!
    private var bonusFeedbackGenerator: UIImpactFeedbackGenerator!
    private var collisionFeedbackGenerator: UIImpactFeedbackGenerator!
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSoundEffects()
        presentScene()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    
    // MARK: - Views
    
    private func presentScene() {
        
        if let scene = SpaceScene(fileNamed: "SpaceScene") {
            
            self.gameScene = scene
            gameScene.scaleMode = .resizeFill
            gameScene.gameSceneDelegate = self
            
            //let transition = SKTransition.crossFade(withDuration: 0.3)
            sceneView.presentScene(gameScene)
        }
        
        #if DEBUG
        sceneView.ignoresSiblingOrder = true
        sceneView.showsFPS = true
        sceneView.showsNodeCount = true
        sceneView.showsPhysics = true
        #endif
    }
    
    
    // MARK: - Sound & effects
    
    private func setupSoundEffects() {
        
        bonusFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
        collisionFeedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
        
        audioPlayer = AVAudioPlayer.createWith(musicFileName: Music.spaceScene.rawValue)
    }
    
    
    // MARK: - Actions
    
    @IBAction func stateButtonTapped(_ sender: UIButton) {
        
        audioPlayer.pause()
        
        gameScene.pauseGame()
        
        performSegue(withIdentifier: "pauseSegue", sender: nil)
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let pauseVC = segue.destination as? PauseViewController {
            
            pauseVC.delegate = self
        }
        
        if let gameOverVC = segue.destination as? GameOverViewController {
            
            gameOverVC.delegate = self
            
            if let score = sender as? Int {
                
                gameOverVC.score = score
            }
        }
    }
}

extension GameViewController: GameSceneDelegate {
    
    func gameDidStart() {
        
        scoreLabel.text = String(0)
        
        audioPlayer.playMusic(needsRestart: true)
    }
    
    func didUpdate(score: Int) {
        
        scoreLabel.text = String(score)
    }
    
    func needTapticFeedback(isHeavy: Bool) {
        
        if DefaultsManager.isVibrationEnabled() {
            
            if isHeavy {
                
                collisionFeedbackGenerator.impactOccurred()
                
            } else {
                
                bonusFeedbackGenerator.impactOccurred()
            }
            
        }
    }
    
    func gameDidEnd(score: Int) {
        
        audioPlayer.stopMusic()
        
        performSegue(withIdentifier: "gameOverSegue", sender: score)
    }
}

extension GameViewController: PauseViewControllerDelegate, GameOverViewControllerDelegate {
    
    func restartButtonTapped() {
        
        presentScene()
    }
    
    func resumeButtonTapped() {
        
        audioPlayer.playMusic(needsRestart: false)
        
        gameScene.continueGame()
    }
    
    func menuButtonTapped() {
        
        gameScene.pauseGame()
        
        dismiss(animated: true, completion: nil)
    }
}
