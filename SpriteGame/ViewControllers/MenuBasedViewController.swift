//
//  MenuBasedViewController.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 15/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import UIKit

class MenuBasedViewController: UIViewController {
    
    @IBOutlet weak var menuContainerView: UIView!
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    
    // MARK: - Views
    
    open func setupViews() {
        
        menuContainerView.alpha = 0
    }
    
    open func updateViews() {
        
    }
    
    open func showMenu() {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.menuContainerView.alpha = 1
        })
    }
    
    open func hideMenu() {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.menuContainerView.alpha = 0
        })
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        hideMenu()
    }
    
    @IBAction func unwind(segue:UIStoryboardSegue) {
        
        showMenu()
    }
}
