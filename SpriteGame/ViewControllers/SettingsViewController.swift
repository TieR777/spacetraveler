//
//  SettingsViewController.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 04/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var musicButton: UIButton!
    @IBOutlet weak var soundButton: UIButton!
    @IBOutlet weak var vibrationButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSettigns()
    }
    
    private func setupSettigns() {
        
        musicButton.isSelected = DefaultsManager.isMusicEnabled()
        soundButton.isSelected = DefaultsManager.isSoundEnabled()
        vibrationButton.isSelected = DefaultsManager.isVibrationEnabled()
    }
    
    
    // MARK: - Actions
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "unwindSegue", sender: nil)
    }
    
    @IBAction func musicButtonTapped(_ sender: Any) {
        
        let isMusicEnabled = DefaultsManager.isMusicEnabled()
        musicButton.isSelected = !isMusicEnabled
        UserDefaults.standard.set(!isMusicEnabled, forKey: DefaultsKeys.settignsMusic.rawValue)
    }
    
    @IBAction func soundButtonTapped(_ sender: Any) {
        
        let isSoundEnabled = DefaultsManager.isSoundEnabled()
        soundButton.isSelected = !isSoundEnabled
        UserDefaults.standard.set(!isSoundEnabled, forKey: DefaultsKeys.settignsSound.rawValue)
    }
    
    @IBAction func vibrationButtonTapped(_ sender: Any) {
        
        let isVibrationEnabled = DefaultsManager.isVibrationEnabled()
        vibrationButton.isSelected = !isVibrationEnabled
        UserDefaults.standard.set(!isVibrationEnabled, forKey: DefaultsKeys.settignsVibration.rawValue)
    }
}
