//
//  Style.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 14/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import Foundation
import UIKit

enum Color: String {
    
    case white = "white"
    case defaultWhite = "defaultWhite"
    case black = "black"
    case defaultBlack = "defaultBlack"
    case red = "red"
    case yellow = "yellow"
    case green = "green"
    
    var uiColor: UIColor {
        
        return UIColor(named: self.rawValue)!
    }
    
    var cgColor: CGColor {
        
        return uiColor.cgColor
    }
}

enum Font: String {
    
    case computer = "Computer Pixel-7"
}

