//
//  Utils.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 09/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import Foundation
import AVFoundation

class Utils {
    
    static func getTempUsername(uid: String?) -> String {
        
        return "SpaceTraveler\(uid?.suffix(4) ?? "1988")"
    }
}

extension AVAudioPlayer {
    
    static func createWith(musicFileName: String) -> AVAudioPlayer {
        
        let audioPath = Bundle.main.path(forResource: musicFileName, ofType: "mp3")
        let audioPlayer = try! AVAudioPlayer(contentsOf: URL(string: audioPath!)!)
        audioPlayer.numberOfLoops = -1
        audioPlayer.prepareToPlay()
        
        return audioPlayer
    }
    
    func playMusic(needsRestart: Bool) {
        
        volume = 0
        
        if needsRestart {
            
            currentTime = 0
        }
        
        play()
        
        if DefaultsManager.isMusicEnabled() {
            
            setVolume(0.5, fadeDuration: 0.5)
        }
    }
    
    func stopMusic() {
        
        setVolume(0, fadeDuration: 0.5)
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.5) {
            [weak self] in
            
            self?.stop()
        }
    }
}
