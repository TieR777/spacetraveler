//
//  Resources.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 14/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import Foundation
import SpriteKit

struct Layers {
    
    let background: CGFloat = 0
    let emitter: CGFloat = 1
    let objects: CGFloat = 2
    let character: CGFloat = 3
}

enum Layer: CGFloat {
    
    case background = -1
    case emitter = 1
    case objects = 2
    case character = 3
    case ui = 5
}

enum NodeCategory: UInt32 {
    
    case none = 0b0000
    case bounds = 0b0001
    case character = 0b0010
    case damaging = 0b0011
    case collectable = 0b0100
}

enum Emitter: String {
    
    case stars = "StarsParticle"
    case explosion = "ExplosionParticle"
    case asteroidTrail = "AsteroidTrailParticle"
    case engine = "EngineParticle"
    case blood = "BloodParticle"
}

enum Music: String {
    
    case mainMenu = "music_menu"
    case spaceScene = "music_space"
}

enum Sound: String {
    
    case bump8Bit = "sound_bump_8bit"
    case coin = "sound_coin"
    case life = "sound_life"
    case punch = "sound_punch"
    case gameOver = "sound_game_over"
    case gameOverVoice = "sound_game_over_voice"
}
