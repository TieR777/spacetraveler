//
//  DatabaseManager.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 10/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class DatabaseManager {
    
    private static var databaseRef: DatabaseReference {
        
        get {
            
            return Database.database().reference()
        }
    }
    
    static func saveLastGame(score: Int, user: User) {
        
        databaseRef.child("score").child(user.uid).observeSingleEvent(of: .value, with: {
            snapshot in
            
            guard let savedScore = snapshot.value as? Int else {
                
                databaseRef.child("score").child(user.uid).setValue(score)
                
                return
            }
            
            if score > savedScore {
                
                databaseRef.child("score").child(user.uid).setValue(score)
            }
        })
    }
    
    static func getLadder(complition: @escaping (([(uid: String, score: Int)]?) -> Void)) {
        
        databaseRef.child("score").queryOrderedByValue().observeSingleEvent(of: .value, with: {
            snapshot in
            
            guard let dictionary = snapshot.value as? Dictionary<String, Int> else {
                
                complition(nil)
                
                return
            }
            
            complition(dictionary.compactMap({
                key, value in
                
                return (key, value)
            }))
            
        }, withCancel: {
            error in
            
            complition(nil)
        })
    }
}
