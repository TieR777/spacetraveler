//
//  DefaultsManager.swift
//  SpriteGame
//
//  Created by Nikita Gorobets on 09/03/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

enum DefaultsKeys: String {
    
    case currentUser = "current_user"
    case settignsMusic = "settings_music"
    case settignsSound = "settings_sound"
    case settignsVibration = "settings_vibration"
}

class DefaultsManager {
    
    static func defaults() -> UserDefaults {
        
        return UserDefaults.standard
    }
    
    static func setCurrentUser(_ user: User) {
        
        DefaultsManager.defaults().set(NSKeyedArchiver.archivedData(withRootObject: user), forKey: DefaultsKeys.currentUser.rawValue)
    }
    
    static func getCurrentUser() -> User? {
        
        guard let userData = DefaultsManager.defaults().data(forKey: DefaultsKeys.currentUser.rawValue), let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? User else {
            
            return nil
        }
        
        return user
    }
    
    static func isMusicEnabled() -> Bool {
        
        return DefaultsManager.defaults().bool(forKey: DefaultsKeys.settignsMusic.rawValue)
    }
    
    static func isSoundEnabled() -> Bool {
        
        return DefaultsManager.defaults().bool(forKey: DefaultsKeys.settignsSound.rawValue)
    }
    
    static func isVibrationEnabled() -> Bool {
        
        return DefaultsManager.defaults().bool(forKey: DefaultsKeys.settignsVibration.rawValue)
    }
}
