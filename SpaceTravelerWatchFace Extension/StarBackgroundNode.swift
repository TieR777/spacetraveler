//
//  StarBackgroundNode.swift
//  SpaceTravelerWatchFace Extension
//
//  Created by Nikita Gorobets on 03/04/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class StarBackgroundNode: SKSpriteNode {
    
    convenience init(size: CGSize) {
        self.init()
        
        self.size = size
    }
    
    func launchStars() {
        
        guard children.isEmpty else { return }
        
        for i in 1...5 {
            
            if let starsEmitter = SKEmitterNode(fileNamed: "StarsWatchParticle") {
                
                starsEmitter.particleTexture = SKTexture(imageNamed: "star\(i)")
                starsEmitter.zPosition = 1
                starsEmitter.position = CGPoint(x: 0, y: size.height / 2 + 16.0 * CGFloat(i))
                starsEmitter.particlePositionRange.dx = size.width
                starsEmitter.advanceSimulationTime(10)
                
                addChild(starsEmitter)
            }
        }
    }
    
    func stopStars() {
        
        children.forEach { $0.removeFromParent() }
    }
}
