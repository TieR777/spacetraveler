//
//  InterfaceController.swift
//  SpaceTravelerWatchFace Extension
//
//  Created by Nikita Gorobets on 03/04/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    @IBOutlet var skInterface: WKInterfaceSKScene!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        if let scene = SpaceWatchScene(fileNamed: "SpaceWatchScene") {
            
            scene.scaleMode = .resizeFill
            
            skInterface.presentScene(scene)
            skInterface.preferredFramesPerSecond = 30
        }
    }
    
    override func willActivate() {
        super.willActivate()
        
        // This method is called when watch view controller is about to be visible to user
    }
    
    override func didAppear() {
        super.didAppear()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
        
        // This method is called when watch view controller is no longer visible
    }
}
