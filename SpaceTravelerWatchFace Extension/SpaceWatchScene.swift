//
//  SpaceWatchScene.swift
//  SpaceTravelerWatchFace Extension
//
//  Created by Nikita Gorobets on 03/04/2019.
//  Copyright © 2019 ng. All rights reserved.
//

import SpriteKit

class SpaceWatchScene: SKScene {
    
    private var starsBackground: StarBackgroundNode!
    private var dateLabel: SKLabelNode = {
        
        let dateLabel = SKLabelNode(fontNamed: Font.computer.rawValue)
        dateLabel.zPosition = 5
        dateLabel.fontSize = 24
        dateLabel.fontColor = Color.white.uiColor
        
        return dateLabel
    }()
    private var timeLabel: SKLabelNode = {
        
        let timeLabel = SKLabelNode(fontNamed: Font.computer.rawValue)
        timeLabel.zPosition = 5
        timeLabel.fontSize = 40
        timeLabel.fontColor = Color.yellow.uiColor
        
        return timeLabel
    }()
    private let dateFormatter: DateFormatter = {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.YY"
        
        return dateFormatter
    }()
    private let timeFormatter: DateFormatter = {
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm:ss"
        
        return timeFormatter
    }()
    private let spaceTravelerNode: SKSpriteNode = {
       
        let spaceTravelerNode = SKSpriteNode(imageNamed: "astronautRight")
        spaceTravelerNode.zPosition = 4
        
        return spaceTravelerNode
    }()
    private let engineNode: SKEmitterNode = {
       
        let engineNode = SKEmitterNode(fileNamed: "EngineWatchParticle")!
        engineNode.zPosition = 1
        
        return engineNode
    }()
    private var isCurrentPositionIsRight = false
    
    override func sceneDidLoad() {
        
        backgroundColor = UIColor.black
        
        starsBackground = StarBackgroundNode(size: size)
        starsBackground.zPosition = -1
        addChild(starsBackground)
        starsBackground.launchStars()
        
        dateLabel.position = CGPoint(x: 0, y: 16)
        addChild(dateLabel)
        timeLabel.position = CGPoint(x: 0, y: -16)
        addChild(timeLabel)
        
        addChild(spaceTravelerNode)
        
        spaceTravelerNode.addChild(engineNode)
        engineNode.targetNode = self
        
        initialDrop()
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        let date = Date()
        dateLabel.text = dateFormatter.string(from: date)
        timeLabel.text = timeFormatter.string(from: date)
    }
    
    private func initialDrop() {
        
        spaceTravelerNode.position = CGPoint(x: frame.minX - frame.minX / 2.0, y: frame.minY - spaceTravelerNode.size.height * 2.0)
        
        let dropAction = SKAction.run {
            
            self.moveSpaceTraveler(isRight: false, isFromDrop: true)
        }
        
        spaceTravelerNode.run(dropAction) {
            
            let switchAction = SKAction.run {
                
                self.moveSpaceTraveler(isRight: !self.isCurrentPositionIsRight, isFromDrop: false)
            }
            
            self.spaceTravelerNode.run(SKAction.repeatForever(SKAction.sequence([SKAction.wait(forDuration: 7), switchAction])))
        }
    }
    
    private func moveSpaceTraveler(isRight: Bool, isFromDrop: Bool) {
        
        let texture = SKTexture(imageNamed: isRight ? "astronautRight" : "astronautLeft")
        spaceTravelerNode.texture = texture
        
        let travelerWidth = spaceTravelerNode.size.width
        let margin: CGFloat = 4.0
        
        var x = isRight ? frame.maxX : frame.minX
        x += isRight ? -travelerWidth : travelerWidth
        x += isRight ? -margin : margin
        
        let y = -size.height / 2.0 + spaceTravelerNode.size.height + margin
        
        let moveAction = SKAction.move(to: CGPoint(x: x, y: y), duration: 1.5)
        
        moveAction.timingMode = .easeInEaseOut
        spaceTravelerNode.run(moveAction) {
            
            self.isCurrentPositionIsRight = isRight
            
            let texture = SKTexture(imageNamed: isRight ? "astronautLeft" : "astronautRight")
            self.spaceTravelerNode.texture = texture
        }
        
        if !isFromDrop {
            
            spaceTravelerNode.run(SKAction.rotate(toAngle: isRight ? -0.3 : 0.3, duration: 1)) {
                
                self.spaceTravelerNode.run(SKAction.rotate(toAngle: 0, duration: 0.5))
            }
        }
        
        if Bool.random() && !isFromDrop {
            
            dropAsteroid(travelerFutureX: x)
        }
    }
    
    private func dropAsteroid(travelerFutureX: CGFloat) {
        
        let asteroidNode = SKSpriteNode(imageNamed: "asteroid\(Int.random(in: 1...3))")
        asteroidNode.zPosition = 2
        asteroidNode.setScale(CGFloat.random(in: 0.8...1.5))
        addChild(asteroidNode)
        
        let trailEmitter = SKEmitterNode(fileNamed: "AsteroidTrailWatchParticle")!
        trailEmitter.zPosition = 1
        asteroidNode.addChild(trailEmitter)
        trailEmitter.targetNode = self
        
        let asteroidWidth = asteroidNode.size.width
        let travelerHalfWidth = spaceTravelerNode.size.width * 1.5
        let xBoundOne = isCurrentPositionIsRight ? frame.maxX - asteroidWidth : frame.minX + asteroidWidth
        let xBoundTwo = isCurrentPositionIsRight ? travelerFutureX + travelerHalfWidth : travelerFutureX - travelerHalfWidth
        let randomXInBounds = xBoundOne > xBoundTwo ? CGFloat.random(in: xBoundTwo...xBoundOne) : CGFloat.random(in: xBoundOne...xBoundTwo)
        
        asteroidNode.position = CGPoint(x: randomXInBounds, y: frame.maxY + asteroidNode.size.height + 16.0)
        
        let moveAction = SKAction.move(to: CGPoint(x: asteroidNode.position.x + CGFloat.random(in: -5.0...5.0), y: frame.minY * 2.0), duration: TimeInterval.random(in: 2.0...3.0))
        moveAction.timingMode = .easeIn
        asteroidNode.run(moveAction) {
            
            asteroidNode.removeFromParent()
        }
    }
}

enum Font: String {
    
    case computer = "Computer Pixel-7"
}

enum Color: String {
    
    case white = "white"
    case defaultWhite = "defaultWhite"
    case black = "black"
    case defaultBlack = "defaultBlack"
    case red = "red"
    case yellow = "yellow"
    case green = "green"
    
    var uiColor: UIColor {
        
        return UIColor(named: self.rawValue)!
    }
    
    var cgColor: CGColor {
        
        return uiColor.cgColor
    }
}
